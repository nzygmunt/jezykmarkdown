# Nagłówek 1

To jest pierwszy paragraf

To jest drugi paragraf

A to jest trzeci paragraf

```py
print("Hello World")
```


**pogrubienie**

~~przekreślenie~~

*kursywa*

lista
1. Jeden
2. Dwa
	1. Pod dwa
3. Trzy
4. Cztery

Lista nienumeryczna
- jeden
- dwa
- trzy
	- pod trzy
	- drugi pod trzy
```py
num1=2
num2=5
result=num1+num2
print(result)
```
![picture.jpeg](picture.jpeg)
